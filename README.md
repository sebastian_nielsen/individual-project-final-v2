# # README # #

### self acknowledgement ###

* Quick summary

AngularJs website for self acknowledgement using Firebase as database and Openshift for hosting
* Version:

Final, http://selfacknowledgement-tempproj.rhcloud.com/#/

### How do I get set up? ###

* Summary of set up

Yeoman is used as scaffolding tool and is required to run the website locally. 

* Configuration/ Dependencies

Visit http://yeoman.io/ for in depth notes about the istallation but in short the following is needed:

npm install -g yo

npm install -g bower

npm install -g grunt

gem update --system

gem install compass

http://rubyinstaller.org/ 

http://git-scm.com/download/win



Make sure that paths for ruby, npm and git and added. See: http://www.computerhope.com/issues/ch000549.htm

Should look like the following:
%CommonProgramFiles%\Microsoft Shared\Windows Live;C:\Users\USER\AppData\Roaming\npm;C:\Ruby21\bin;C:\Program Files (x86)\Git\bin;C:\Program Files\nodejs\;

* Deployment instructions

To start coding you can simply create a new yeoman by (using: npm install generator-angularfire) on your local path (cd c:\folder\subfolder).
After this step run grunt. This will complete with errors and you should now be able to run grunt serve to start you local web server for debugging. 
You can clon this git to you local git or copy it manually by downloading the repository. 

### Contribution guidelines ###
* Free to edit and use the software.


### Who do I talk to? ###

* Repo owner or admin
-contact: sebbe605@gmail.com