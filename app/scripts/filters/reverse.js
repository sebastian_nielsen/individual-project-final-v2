'use strict';
// Reverse array items from node child to parent
angular.module('selfAcknowledgementApp')
  .filter('reverse', function() {
    return function(items) {
      return angular.isArray(items)? items.slice().reverse() : [];
    };
  });
