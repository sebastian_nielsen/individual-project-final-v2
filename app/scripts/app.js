'use strict';

/**
 * @ngdoc overview
 * @name selfAcknowledgementApp
 * @description
 * # selfAcknowledgementApp
 *
 * Main module of the application.
 */
angular.module('selfAcknowledgementApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.utils',
    'simpleLogin'
  ]);