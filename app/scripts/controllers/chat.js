'use strict';
/**
 * @ngdoc function
 * @name selfAcknowledgementApp.controller:MypageCtrl
 * @description
 * # MypageCtrl
 * Provides add post/ remove and edit for auth users
 */
angular.module('selfAcknowledgementApp')
  .controller('MypageCtrl', function ($scope, user, simpleLogin, fbutil, $timeout) {
    $scope.user = user;
    $scope.logout = simpleLogin.logout;
    $scope.messages = [];
    loadProfile(user);
    var date = new Date();
    var chatmessage = "What good did you do today?";
    $scope.textBoxMessage = chatmessage;
    var list = fbutil.syncArray('posts/'+user.uid);

    function loadProfile(user) {
      if( $scope.profile ) {
        $scope.profile.$destroy();
      }
      fbutil.syncObject('users/'+user.uid).$bindTo($scope, 'profile');
    }

    var ref = new Firebase("https://boostme.firebaseio.com/");
    // Ref to child node
    var userRef = ref.child('posts');

    // Add new post
    $scope.addMessage = function(){
      if($scope.newMessage == ""){
        $scope.textBoxMessage = "No message to submit. Please enter a message.";
      }
      else{
        // Format the date stamp to yyMMdd standard
        var yyyy = date.getFullYear().toString();
        var trimyyyy = yyyy.substr(2);
        var mm = (date.getMonth()+1).toString();
        var dd = date.getDate().toString();
        var yyMMdd = trimyyyy + mm + dd;

        userRef.child(user.uid).push({
          date: yyMMdd,
          post: $scope.newMessage
        })
      }
    }

    // Remove a post from user
    $scope.removeMe = function(message){
      list.$remove(message);
    }

    // Edit a post
    $scope.editMe = function(message) {
      if($scope.newMessage == null){
        $scope.textBoxMessage = "Here you can edit your post by entering a new message and pressing edit on the affected post" + "\n \n" + "Your post:" + "\n" + message.post;
      }
      else{
        message.post = $scope.newMessage;
        list.$save(message);
        $scope.textBoxMessage = chatmessage;
        $scope.newMessage = null;
      }
    }

    console.log(list);
    $scope.messages = list;

    $scope.messages.$loaded().catch(alert);

    function alert(msg) {
      $scope.err = msg;
      $timeout(function() {
        $scope.err = null;
      }, 5000);
    }

});
