'use strict';
/**
 * @ngdoc function
 * @name selfAcknowledgementApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the selfAcknowledgementApp
 */
angular.module('selfAcknowledgementApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
